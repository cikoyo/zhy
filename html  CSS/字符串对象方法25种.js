//1.返回在指定位置的字符。
var str = "HELLO WORLD";
console.log(str.charAt(0))

//2.返回在指定的位置的字符的 Unicode 编码。
var str = "HELLO WORLD";
console.log(str.charCodeAt(0))

//3.连接两个或更多字符串，并返回新的字符串。
var txt1 = "Hello ";
var txt2 = "world!";
//var n = txt1.concat(txt2);
var m = `${txt1}${txt2}`
console.log(m)


//4.判断当前字符串是否是以指定的子字符串结尾的（区分大小写）。
var str = "HELLO WORLD";
console.log(str.endsWith("WORLD"))

//5.将 Unicode 编码转为字符。
var n = String.fromCharCode(72, 69, 76, 76, 79);
console.log(n)

//6.返回某个指定的字符串值在字符串中首次出现的位置。
var str = "Hello world, welcome to the universe.";
var n = str.indexOf("welcome");
console.log(n)

//7.查找字符串中是否包含指定的子字符串。
var str = "Hello world, welcome to the Runoob.";
var n = str.includes("World");
console.log(n)

//8.从后向前搜索字符串，并从起始位置（0）开始计算返回字符串最后出现的位置。
var str = "I am from runoob，welcome to runoob site.";
var n = str.lastIndexOf("runoob");
console.log(n)

//9.查找找到一个或多个RegExp正则表达式的匹配。正则表达式描述了字符的模式
//i - 修饰符是用来执行不区分大小写的匹配。
//g - 修饰符是用于执行全文的搜索
var str = "The rain in SPAIN stays mainly in the plain";
var n = str.match(/ain/ig);
console.log(n)

//10.复制字符串指定次数，并将它们连接在一起返回。
var str = "Runoob";
console.log(str.repeat(4))

//11.在字符串中查找匹配的子串，并替换与正则表达式匹配的子串。
var str = "Visit Microsoft!Visit Microsoft!"
var n = str.replace("Microsoft", "Runoob");
console.log(n)


//12.在字符串中查找匹配的子串，并替换与正则表达式匹配的所有子串。
var str = "Visit Microsoft!Visit Microsoft!"
var n = str.replace(/Microsoft/g, "Runoob");
console.log(n)

//13.查找与正则表达式相匹配的值。
var str = "Runoob Visit   Runoob!";
var n = str.search("Runoob");
console.log(n)

//14.提取字符串的片断，并在新的字符串中返回被提取的部分。
var str = "Hello world!";
var n = str.slice(3,7);
console.log(n)


//15.把字符串分割为字符串数组。
var str="How are you doing today?";
var n=str.split(" ");
console.log(n)


//16.查看字符串是否以指定的子字符串开头。
var str = "Hello world, welcome to the Runoob.";
  var n = str.startsWith("Hello");
  console.log(n)

//17.从起始索引号开始提取字符串中指定数目的字符。
var str="Hello world!";
var n=str.substr(2,3);
console.log(n)

//18.提取字符串中两个指定的索引号之间的字符。
var str="Hello world!";
console.log(str.substring(3,7));

//19.把字符串转换为小写。把字符串转换为大写。
var txt="Runoob";
console.log(txt.toLowerCase());
console.log(txt.toUpperCase());

//20.去除字符串两边的空白。  
var str = "     Runoob     ";
console.log(str.trim());

//21.根据本地主机的语言环境把字符串转换为小写。
var str = "Runoob";
var res = str.toLocaleLowerCase();
console.log(res)

//22.根据本地主机的语言环境把字符串转换为大写。
var str = "Runoob";
  var res = str.toLocaleUpperCase();
  console.log(res)


  //23.返回某个字符串对象的原始值。
  var str="Hello world!";
  console.log(str.valueOf());

//24.返回一个字符串。
var str = "Runoob";
var res = str.toString();
console.log(res)






















