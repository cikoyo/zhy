const SampleClass = class {
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }
    sum() {
        return this.a + this.b;
    }
}
let object = new SampleClass(10, 20);
console.log(object.sum());


function myFunction(a, b) {
    return a + b;
}
console.log(myFunction(10, 20))