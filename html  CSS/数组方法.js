//1.合并两个数组
var hege = ["Cecilie", "Lone"];
var stale = ["Emil", "Tobias", "Linus"];
var children = hege.concat(stale);
console.log(children)

//2.合并三个数组
var parents = ["Jani", "Tove"];
var brothers = ["Stale", "Kai Jim", "Borge"];
var children = ["Cecilie", "Lone"];
var sister = ["Cecie", "Lon"];

var family = parents.concat(brothers, children, sister);
console.log(family)

//3.用数组的元素组成字符串
var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.join())

//4.删除数组的最后一个元素
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.pop();
console.log(fruits)

//5.数组的末尾添加新的元素
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.push("Kiwi")
console.log(fruits)

//6.将一个数组中的元素的顺序反转排序
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.reverse();
console.log(fruits)

//7.删除数组的第一个元素
var fruits = ["Banana", "Orange", "Apple", "Mango"];
var delell = fruits.shift();
console.log(fruits)

//8.从一个数组中选择元素 
var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
	var citrus = fruits.slice(1,3);
    console.log(citrus)

//9.数组排序（按字母顺序升序）
var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.sort();
    console.log(fruits)


//10.数字排序（按数字顺序升序）
var points = [40,100,1,5,25,10];
	points.sort(function(a,b){return a-b});
    console.log(points)

//11.数字排序（按数字顺序降序）
var points = [40,100,1,5,25,10];
	points.sort(function(a,b){return b-a});
    console.log(points)

//12.在数组的第2位置添加一个元素 0不删除该位置元素
var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.splice(2,0,"Lemon","Kiwi");
    console.log(fruits)

//13.转换数组到字符串
var fruits = ["Banana", "Orange", "Apple", "Mango"];
	var str = fruits.toString();
    console.log(str)

//14.在数组的开头添加新元素
var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.unshift("Lemon","Pineapple");
    console.log(fruits)



